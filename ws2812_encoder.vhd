library ieee;
use ieee.std_logic_1164.all;

entity ws2812_encoder is
	generic (
		DRIVERS : positive;
		CYCLES_0H : positive;
		CYCLES_0L : positive;
		CYCLES_1H : positive;
		CYCLES_1L : positive
	);
	port (
		n_reset : in std_logic;
		clk     : in std_logic;

		run   : in std_logic;
		ready : out std_logic;

		data_in     : in std_logic_vector(DRIVERS-1 downto 0);
		encoded_out : out std_logic_vector(DRIVERS-1 downto 0)
	);
end ws2812_encoder;

architecture rtl of ws2812_encoder is
	signal counter : natural;
	signal current_data : std_logic_vector(DRIVERS-1 downto 0);
begin
	assert DRIVERS = 1 or CYCLES_0H + CYCLES_0L = CYCLES_1H + CYCLES_1L
		report "When using more than one driver, 0 and 1 bits must have the same length"
		severity failure;

	encode: process(n_reset, clk)
	begin
		if n_reset /= '1' then
			counter <= 0;
			encoded_out <= (others => '0');
		elsif rising_edge(clk) then
			if counter /= 0 or run = '1' then
				counter <= counter + 1;
			end if;

			if counter = 0 and run = '1' then
				-- sample data so we can reuse it when changing from high to low
				current_data <= data_in;
				encoded_out <= (others => '1');
			end if;

			if counter = CYCLES_0H then
				for i in 0 to DRIVERS-1 loop
					if current_data(i) = '0' then
						encoded_out(i) <= '0';
					end if;
				end loop;
			end if;

			if counter = CYCLES_1H then
				for i in 0 to DRIVERS-1 loop
					if current_data(i) = '1' then
						encoded_out(i) <= '0';
					end if;
				end loop;
			end if;

			if (current_data(0) = '0' and counter = CYCLES_0H + CYCLES_0L - 2)
			  or (current_data(0) = '1' and counter = CYCLES_1H + CYCLES_1L - 2) then
				counter <= 0;
			end if;
		end if;
	end process;

	ready <= '1' when counter = 0 else '0';
end rtl;
