library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.ws2812_pkg.all;

entity ws2812_parallel is
	generic (
		NUM_DRIVERS : positive;
		NUM_LEDS : natural;
		COLOR_ORDER : string(1 to 3);
		T_CLK : time;

		T0H : time;
		T0L : time;
		T1H : time;
		T1L : time;

		T_RES : time
	);
	port (
		n_reset : in std_logic;
		clk : in std_logic;
		run : in std_logic := '1';
		done : out std_logic;

		led_addr  : out std_logic_vector(addr_bits_req(NUM_LEDS)-1 downto 0) := (others => '0');

		led_colors : in colors_vector(NUM_DRIVERS-1 downto 0);

		dout : out std_logic_vector(NUM_DRIVERS-1 downto 0)
	);
end entity;

architecture rtl of ws2812_parallel is
	constant CYCLES_0H  : natural := num_cycles(T_CLK, T0H);
	constant CYCLES_0L  : natural := num_cycles(T_CLK, T0L);
	constant CYCLES_1H  : natural := num_cycles(T_CLK, T1H);
	constant CYCLES_1L  : natural := num_cycles(T_CLK, T1L);
	constant CYCLES_RES : natural := num_cycles(T_CLK, T_RES);

	constant BITS_PER_LED : natural := 24;

	signal countdown : natural;

	-- the address of the next LED, used to fetch its data
	signal next_address : natural range 0 to NUM_LEDS-1;
	-- marker to enter reset state after this LED
	signal is_last_led  : std_logic;

	subtype bit_index_t is natural range BITS_PER_LED-1 downto 0;

	-- the index of the next bit to be transmitted for this set of LEDs
	signal curr_bit : bit_index_t;

	-- data for the current set of LEDs - gets shifted left for each bit
	-- FIXME name
	type data_t is array(bit_index_t) of std_logic_vector(NUM_DRIVERS-1 downto 0);
	signal curr_data : data_t;

	signal encoder_ready : std_logic;
	signal encoder_run   : std_logic;
	signal encoder_in    : std_logic_vector(NUM_DRIVERS-1 downto 0);
begin
	encoder: entity work.ws2812_encoder
		generic map (
			DRIVERS => NUM_DRIVERS,
			CYCLES_0H => CYCLES_0H,
			CYCLES_0L => CYCLES_0L,
			CYCLES_1H => CYCLES_1H,
			CYCLES_1L => CYCLES_1L
		)
		port map (
			n_reset => n_reset,
			clk => clk,

			run => encoder_run,
			ready => encoder_ready,

			data_in => encoder_in,
			encoded_out => dout
		);

	led_addr <= std_logic_vector(to_unsigned(next_address, led_addr'length));
	encoder_in <= curr_data(BITS_PER_LED-1);

	process(n_reset, clk)
		variable color_data : std_logic_vector(bit_index_t);
	begin
		if n_reset /= '1' then
			countdown <= CYCLES_RES;
			is_last_led <= '1';
			next_address <= 0;
			curr_bit <= 0;
		elsif rising_edge(clk) then
			encoder_run <= '0';
			done <= '0';

			if is_last_led = '1' and curr_bit = 0 then
				-- we're on the last LED and done with the last bit, enter reset timeout
				if countdown /= 0 then
					countdown <= countdown - 1;
				elsif run = '1' then
					is_last_led <= '0';
				else
					done <= '1';
				end if;
			elsif encoder_ready = '1' and encoder_run = '0' then
				-- ready to transmit next bit, and haven't
				-- already done so in last cycle
				encoder_run <= '1';

				if curr_bit = 0 then
					-- done with the last bit, prepare for next LED
					if next_address = NUM_LEDS-1 then
						-- last LED comes next, mark this so we can enter idle after
						is_last_led <= '1';
						countdown <= CYCLES_RES;
						next_address <= 0;
					else
						next_address <= next_address + 1;
					end if;

					-- latch data for next LED
					for driver in 0 to NUM_DRIVERS-1 loop
						color_data := order_colors(
							led_colors(driver),
							COLOR_ORDER
						);
						for bit_n in bit_index_t loop
							curr_data(bit_n)(driver) <= color_data(bit_n);
						end loop;
					end loop;
					curr_bit <= BITS_PER_LED-1;
				else
					-- shift data left one bit
					curr_data <= curr_data(BITS_PER_LED-2 downto 0) & std_logic_vector'(NUM_DRIVERS-1 downto 0 => '0');
					curr_bit <= curr_bit - 1;
				end if;
			end if;
		end if;
	end process;
end rtl;
