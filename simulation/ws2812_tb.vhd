library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity ws2812_tb is
end ws2812_tb;

architecture behaviour of ws2812_tb is
	signal clk, reset, run, stop : std_logic;
	signal n_reset : std_logic;

	signal led_addr : std_logic_vector(1 downto 0);
	signal led_red, led_green, led_blue : std_logic_vector(7 downto 0);
	signal led_color : std_logic_vector(23 downto 0);
	signal data_out : std_logic;
begin
	uut: entity work.ws2812
		generic map (
			NUM_LEDS => 3,
			COLOR_ORDER => "GRB",
			T_CLK => 50 ns,

			T0H => 0.35 us,
			T0L => 0.8 us,
			T1H => 0.7 us,
			T1L => 0.6 us,

			T_RES => 50 us
		)
		port map (
			n_reset => n_reset,
			clk => clk,
			run => run,

			led_addr => led_addr,

			led_red   => led_red,
			led_green => led_green,
			led_blue  => led_blue,

			dout => data_out
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 25 ns,
			T_period => 50 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	n_reset <= not reset;
	led_red   <= led_color(23 downto 16);
	led_green <= led_color(15 downto 8);
	led_blue  <= led_color(7 downto 0);

	with led_addr select led_color <=
		x"FF0000" when "00",
		x"00FFFF" when "01",
		x"555555" when others;

	stimulus: process
	begin
		run <= '0';
		stop <= '0';
		reset <= '0';

		wait for 100 us;
		run <= '1';
		wait for 1 us;
		run <= '0';

		wait for 300 us;
		run <= '1';
		wait for 300 us;

		stop <= '1';
		wait;
	end process;
end behaviour;
