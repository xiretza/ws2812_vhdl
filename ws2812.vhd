library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.ws2812_pkg.all;

entity ws2812 is
	generic (
		NUM_LEDS : natural;
		COLOR_ORDER : string(1 to 3);
		T_CLK : time;

		T0H : time;
		T0L : time;
		T1H : time;
		T1L : time;

		T_RES : time
	);
	port (
		n_reset : in std_logic;
		clk : in std_logic;
		run : in std_logic := '1';
		done : out std_logic;

		led_addr  : out std_logic_vector(addr_bits_req(NUM_LEDS)-1 downto 0) := (others => '0');
		led_red   : in std_logic_vector(7 downto 0);
		led_green : in std_logic_vector(7 downto 0);
		led_blue  : in std_logic_vector(7 downto 0);

		dout : out std_logic
	);
end ws2812;

architecture rtl of ws2812 is
begin
	inner: entity work.ws2812_parallel generic map (
		NUM_DRIVERS => 1,
		NUM_LEDS => NUM_LEDS,
		COLOR_ORDER => COLOR_ORDER,
		T_CLK => T_CLK,

		T0H => T0H,
		T0L => T0L,
		T1H => T1H,
		T1L => T1L,

		T_RES => T_RES
	) port map (
		n_reset => n_reset,
		clk => clk,
		run => run,
		done => done,

		led_addr => led_addr,

		led_colors => (0 => (red => led_red, green => led_green, blue => led_blue)),

		dout(0) => dout
	);
end rtl;
