library ieee;
use ieee.std_logic_1164.all;

package ws2812_pkg is
	type color_t is record
		red : std_logic_vector(7 downto 0);
		green : std_logic_vector(7 downto 0);
		blue : std_logic_vector(7 downto 0);
	end record;

	type colors_vector is array(natural range <>) of color_t;

	function addr_bits_req(num_elements : integer) return integer;
	function num_cycles(cycle_time, target_time : time) return natural;
	function order_colors(color: color_t; order: string(1 to 3)) return std_logic_vector;
end package ws2812_pkg;

package body ws2812_pkg is
	-- returns the number of address bits required to store a given number of elements - ceil(log2(num))
	function addr_bits_req(num_elements : integer) return integer is
		variable bits_req : integer;
		variable remaining : integer;
	begin
		bits_req := 0;
		remaining := num_elements - 1;

		while remaining > 0 loop
			bits_req := bits_req + 1;
			remaining := remaining / 2;
		end loop;

		return bits_req;
	end addr_bits_req;

	-- return the number of cycles of length cycle_time that
	-- gets closest to a total length of target_time
	function num_cycles(cycle_time, target_time : time)
		return natural is
	begin
		return (target_time + cycle_time / 2) / cycle_time;
	end num_cycles;

	-- order separate color SLVs into an assembled
	-- vector according to an ordering string
	function order_colors(
		color: color_t;
		order : string(1 to 3)) return std_logic_vector is
	begin
		case order is
			when "RGB" =>
				return color.red & color.green & color.blue;
			when "RBG" =>
				return color.red & color.blue & color.green;
			when "GRB" =>
				return color.green & color.red & color.blue;
			when "GBR" =>
				return color.green & color.blue & color.red;
			when "BRG" =>
				return color.blue & color.red & color.green;
			when "BGR" =>
				return color.blue & color.green & color.red;
			when others =>
				report "Bad color ordering: " & order severity error;
		end case;
	end order_colors;
end package body ws2812_pkg;
