library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

-- example implementation for ws2812 driver, simple red LED running along the
-- entire length of the chain
--
-- 50MHz clock is divided down to 25Hz, which then increments light_pos

entity running_light is
	port (
		clock_50mhz : in std_logic;

		n_reset  : in std_logic;
		data_out : out std_logic
	);
end running_light;

architecture behaviour of running_light is
	constant NUM_LEDS : natural := 60;

	signal led_addr  : std_logic_vector(5 downto 0);
	signal led_color : std_logic_vector(23 downto 0);

	signal light_pos : natural range 0 to NUM_LEDS-1;

	subtype divider_t is natural range 0 to 2000000;
	signal move_divider : divider_t;
begin
	ws2812: entity work.ws2812
		generic map (
			NUM_LEDS => NUM_LEDS,
			COLOR_ORDER => "GRB",
			T_CLK => 20 ns,

			T0H => 0.35 us,
			T0L => 0.8 us,
			T1H => 0.7 us,
			T1L => 0.6 us,

			T_RES => 80 us
		)
		port map (
			n_reset => n_reset,
			clk     => clock_50mhz,

			led_addr => led_addr,

			led_red   => led_color(23 downto 16),
			led_green => led_color(15 downto 8),
			led_blue  => led_color(7 downto 0),

			dout => data_out
		);

	-- only the currently active LED is red, all others are off
	led_color <= x"FF0000" when to_integer(unsigned(led_addr)) = light_pos else x"000000";

	-- clock divider
	move: process(n_reset, clock_50mhz)
	begin
		if n_reset /= '1' then
			move_divider <= divider_t'high;
			light_pos <= 0;
		elsif rising_edge(clock_50mhz) then
			if move_divider = 0 then
				move_divider <= divider_t'high;
				if light_pos = divider_t'high then
					light_pos <= 0;
				else
					light_pos <= light_pos + 1;
				end if;
			else
				move_divider <= move_divider - 1;
			end if;
		end if;
	end process;
end behaviour;
