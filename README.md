# Customizable WS2812 driver in VHDL

Top-level entity: `ws2812`

## Generics

Generic       | Explanation
--------------|------------
`NUM_LEDS`    | The number of LEDs in the LED strip
`COLOR_ORDER` | The order in which the color components are transferred, e.g. `"RGB"`, `"GRB"`
`T_CLK`       | The clock period (e.g. `20 ns`)
`T0H`         | The logical 0 high period
`T0L`         | The logical 0 low period
`T1H`         | The logical 1 high period
`T1L`         | The logical 1 low period
`T_RES`       | The reset period

## Interface

The controller is designed to be attached to some external logic providing color
information for specific LED indices (either a memory or some algorithm).

`led_addr` contains the next LED's index, from which an appropriate value for
`led_red`, `led_green`, and `led_blue` has to be calculated.

`d_out` is the binary output signal driving the first LED's `D_IN` input.

`run` can be used to only start streaming out the pixel data on command. After the reset
timeout, sending will only restart once `run` is high.

## Example

`example/running_light.vhd` provides an example implementation of the controller.
The "algorithm" in this case is a simple equality comparison, such that only one
specific LED is active at a time.
